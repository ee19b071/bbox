#See LICENSE.iitm for license details
'''

Author   : Santhosh Pavan
Email id : santhosh@mindgrovetech.in
Details  : This file consists reference model which is used in verifying the bbox design (DUT).

--------------------------------------------------------------------------------------------------
'''
'''
TODO:
Task Description: Add logic for all instructions. One instruction is implemented as an example. 
                  Note - The value of instr (ANDN) is a temp value, it needed to be changed according to spec.

Note - if instr has single operand, take rs1 as an operand
'''

#Reference model
def bbox_rm(instr, instr_name, rs1, rs2, XLEN):
    
    # if instr == 1:
    #     res = rs1 & ~rs2
    #     valid = '1'
    ## logic for all other instr starts 

    ##elif instr == 2:

    opcode = bin(instr)[2:]
    opcode = (22 - len(opcode))*'0' + opcode

    if instr_name == "adduw":                          #add.uw present only in RV64
        if XLEN == 64:
            res = rs1 + (rs2 +2**XLEN)%2**32
            valid = '1'
        else:
            valid = '0'
    elif instr_name == "andn":                        #andn
        res = rs1 & ~rs2
        valid = '1'
    elif instr_name == "bclr":                        #bclr
        index = rs2&(XLEN-1)
        res = rs1 & ~(1 << index)
        valid = '1'
    elif instr_name == "bclri":                        #bclri
        shamt = int(opcode[6:12], 2)
        index = shamt&(XLEN-1)
        res = rs1 & ~(1 << index)
        valid = '1'
    elif instr_name == "bext":                        #bext
        index = rs2&(XLEN-1)
        res = (rs1>>index) & 1
        valid = '1'
    elif instr_name == "bexti":                        #bexti
        shamt = int(opcode[6:12], 2)
        index = shamt&(XLEN-1)
        res = (rs1>>index) & 1
        valid = '1'
    elif instr_name == "binv":                        #binv
        index = rs2&(XLEN-1)
        res = rs1 ^ (1 << index)
        valid = '1'
    elif instr_name == "binvi":                        #binvi
        shamt = int(opcode[6:12], 2)
        index = shamt&(XLEN-1)
        res = rs1 ^ (1 << index)
        valid = '1'
    elif instr_name == "bset":                        #bset
        index = rs2&(XLEN-1)
        res = rs1 | (1 << index)
        valid = '1'
    elif instr_name == "bseti":                        #bseti
        shamt = int(opcode[6:12], 2)
        index = shamt&(XLEN-1)
        res = rs1 | (1 << index)
        valid = '1'
    elif instr_name == "clmul":                        #clmul
        res = 0
        for i in range(XLEN):
            if ((rs2 >> i) & 1):
                res = res ^ (rs1 << i)
        res = res&(2**XLEN-1)
        valid = '1'
    elif instr_name == "clmulh":                        #clmulh
        res = 0
        for i in range(1, XLEN):
            if ((rs2 >> i) & 1):
                res = res ^ (rs1 >> (XLEN-i))
        res = res&(2**XLEN-1)
        valid = '1'
    elif instr_name == "clmulr":                        #clmulr
        res = 0
        for i in range(XLEN-1):
            if ((rs2 >> i) & 1):
                res = res ^ (rs1 >> (XLEN-i-1))
        res = res&(2**XLEN-1)
        valid = '1'
    elif instr_name == "clz":                        #clz
        res = -1
        for i in range(XLEN-1, -1, -1):
            if ((rs1 >> i) & 1) == 1:
                res = i
                break
        res = (XLEN - 1) - res
        valid = '1'
    elif instr_name == "clzw":                        #clzw present only in RV64
        if XLEN == 64:
            res = -1
            for i in range(31, -1, -1):
                if ((rs1 >> i) & 1) == 1:
                    res = i
                    break
            res = 31 - res
            valid = '1'
        else:
            valid = '0'
    
    elif instr_name == 'cpop':                       #cpop
        if XLEN == 64 or XLEN == 32:
            res=0
            for i in range(XLEN):
                if((rs1>>i) &1 == 1):
                    res+=1
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'cpopw':                     #cpopw
        if XLEN == 64:
            res=0
            for i in range(32):
                if((rs1>>i) &1 == 1):
                    res+=1
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'ctz':                       #ctz
        if XLEN == 64 or XLEN == 32:
            res=0
            for i in range(XLEN):
                if((rs1>>i) &1 == 0):
                    res+=1
                else:
                    break
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'ctzw':                      #ctzw
        if XLEN == 64:
            res=0
            for i in range(32):
                if((rs1>>i) &1 ==0):
                    res+=1
                else:
                    break
            valid = '1'
        else:
            valid = '0'


    elif instr_name == 'orcb':                      #orcb
        res=0
        for i in range(int((XLEN-8)/8)+1):
            if((rs1 & 0xff)!=0):
                res |= 0xff << (8*i)
            else: 
                res |= 0x00 << (8*i)
            rs1 = (rs1 >> 8)
        valid='1'

    elif instr_name == 'orn':                       #orn
        res = (rs1) | (~rs2 + (1 << XLEN))
        valid='1'

    elif instr_name == 'rev8':                      #rev8
        res=0
        for i in range(0, int((XLEN-8)/8)+1):
            res = res << 8
            res |= ((rs1 >> 8*i)) & 0xff
        valid = '1'
	

    elif instr_name == 'rol':                       #rol
        if XLEN == 32:
            shamt = rs2 & 0x1f
            res = (rs1 << shamt) | (rs1 >> (XLEN-shamt))
            valid = '1'
        elif XLEN == 64:
            shamt = rs2 & 0x3f
            res = (rs1 << shamt) | (rs1 >> (XLEN-shamt))
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'rolw':                       #rolw
        if XLEN == 64:
            rs1_data = (rs1 & 0xffffffff)
            shamt = rs2 & 0x1f
            result_temp = ((rs1_data << shamt) | (rs1_data >> (32 - shamt))) & 0xffffffff
            res =  ((((result_temp & 0xA0000000) >> 31) * 0xffffffff) << 32) | result_temp
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'ror':                         #ror
        if XLEN == 32:
            shamt = rs2 & 0x1f
            res = (rs1 >> shamt) | (rs1 << (XLEN-shamt))
            valid = '1'
        elif XLEN == 64:
            shamt = rs2 & 0x3f
            res = (rs1 >> shamt) | (rs1 << (XLEN-shamt))
            valid = '1'
        else:
            valid = '0'

    elif instr_name == 'rori':                         #rori
        if XLEN == 32:
            shamt = int(opcode[7:12], 2)
            res = (rs1 >> shamt) | (rs1 << (XLEN-shamt))
            valid = '1'
        elif XLEN == 64:
            shamt = int(opcode[6:12], 2)
            res = (rs1 >> shamt) | (rs1 << (XLEN-shamt))
            valid = '1'
        else:
            valid = '0'
        
    elif instr_name == 'roriw':                        #roriw
        if XLEN == 64:
            shamt = int(opcode[7:12], 2)
            rs1_data = (rs1 & 0xffffffff)
            result_temp = ((rs1_data >> shamt) | (rs1_data << (32 - shamt))) & 0xffffffff
            res =  ((((result_temp & 0xA0000000) >> 31) * 0xffffffff) << 32) | result_temp
            valid = '1'
        else:
            valid = '0'
    
    elif instr_name == 'rorw':                          #rorw
        if XLEN == 64:
            shamt = rs2 & 0x1f
            rs1_data = (rs1 & 0xffffffff)
            result_temp = ((rs1_data >> shamt) | (rs1_data << (32 - shamt))) & 0xffffffff
            res =  ((((result_temp & 0xA0000000) >> 31) * 0xffffffff) << 32) | result_temp
            valid = '1'
        else:
            valid = '0'
    
    elif instr_name == 'sextb':                          #sextb
        rs1_data = rs1 & 0xff
        if XLEN == 64:
            res = ((((rs1_data & 0xA0) >> 7) * 0xffffffffffffff) << 8) | rs1_data
        elif XLEN == 32:
            res = ((((rs1_data & 0xA0) >> 7) * 0xffffff) << 8) | rs1_data
        valid = '1'

    elif instr_name == 'sexth':                          #sexth
        rs1_data = rs1 & 0xffff
        if XLEN == 64:
            res = ((((rs1_data & 0xA000) >> 15) * 0xffffffffffff) << 16) | rs1_data
        elif XLEN == 32:
            res = ((((rs1_data & 0xA000) >> 15) * 0xffff) << 16) | rs1_data
        valid = '1'

    elif instr_name == 'sh1add':                          #sh1add
        res = rs2 + (rs1 << 1)
        valid = '1'

    elif instr_name == 'sh1adduw':                         #sh1adduw
        res = rs2 + ((rs1 & 0xffffffff) << 1)
        valid = '1'

    elif instr_name == 'sh2add':
        res = rs2 + (rs1 << 2)
        valid = '1'

    elif instr_name == 'sh2adduw':                         #sh2adduw
        res = rs2 + ((rs1 & 0xffffffff) << 2)
        valid = '1'

    elif instr_name == 'sh3add':                           #sh3add
        res = rs2 + (rs1 << 3)
        valid = '1'

    elif instr_name == 'sh3adduw':                         #sh3adduw
        res = rs2 + ((rs1 & 0xffffffff) << 3)
        valid = '1'

    elif instr_name == 'slliuw':                           #slliuw
        shamt = int(opcode[6:12], 2)
        rs1_data = (rs1 & 0xffffffff)
        res = rs1_data << shamt
        valid = '1'

    elif instr_name == 'xnor':                             #xnor
        temp = ~(rs1 ^ rs2)
        res = temp + (1 << XLEN)
        valid = '1'

    elif instr_name == 'zexth':                            #zexth
        res = (rs1 & 0xffff)
        valid = '1'

    elif instr_name == 'max':                              #max
        if ((rs1>>(XLEN-1))&1) == 0b1:
            rs1 = rs1 - 2**XLEN
        if ((rs2>>(XLEN-1))&1) == 0b1:
            rs2 = rs2 - 2**XLEN
        if rs1 < rs2:
            res = rs2
        else:
            res = rs1
        if res < 0:
            res = res+2**XLEN
        valid = '1'

    elif instr_name == 'maxu':                             #maxu
        if rs1 < rs2:
            res = rs2
        else:
            res = rs1
        valid = '1'

    elif instr_name == 'min':                              #min
        if ((rs1>>(XLEN-1))&1) == 0b1:
            rs1 = rs1 - 2**XLEN
        if ((rs2>>(XLEN-1))&1) == 0b1:
            rs2 = rs2 - 2**XLEN
        if rs1 < rs2:
            res = rs1
        else:
            res = rs2
        if res < 0:
            res = res+2**XLEN
        valid = '1'

    elif instr_name == 'minu':                             #minu
        if rs1 < rs2:
            res = rs1
        else:
            res = rs2
        valid = '1'

    ## logic for all other instr ends
    else:
        res = 0
        valid = '0'

    if XLEN == 32:
        result = '{:032b}'.format(res)[-32:]
    elif XLEN == 64:
        result = '{:064b}'.format(res)[-64:]

    return valid+result

