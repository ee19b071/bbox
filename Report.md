# Report for BitManip Extension project

###### Members: AMS Arun Krishna (EE19B001), Arun D A (EE19B071), Renish Israeli (EE19B144)

#### Instructuctions to run the tests
1. Clone the repository
2. For RV64:
    - Ensure that BSCDEFINES in makefile and base in test_bbox.py are set to RV64
3. For RV32:
    - Ensure that BSCDEFINES in makefile and base in test_bbox.py are set to RV32
4. Run the following commands:
```bash
$ make clean_build
$ make simulate
```

#### Test run report:
- RV32
```bash
                     ****************************************************************************************
                     ** TEST                            STATUS  SIM TIME (ns)  REAL TIME (s)  RATIO (ns/s) **
                     ****************************************************************************************
                     ** test_bbox.TB_001                 PASS          30.00           0.01       2821.18  **
                     ** test_bbox.TB_002                 PASS          31.00           0.01       3459.47  **
                     ** test_bbox.TB_003                 PASS          31.00           0.01       2966.57  **
                     ** test_bbox.TB_004                 PASS          31.00           0.01       2779.85  **
                     ** test_bbox.TB_005                 PASS          31.00           0.01       2559.20  **
                     ** test_bbox.TB_006                 PASS          31.00           0.01       2801.35  **
                     ** test_bbox.TB_007                 PASS          31.00           0.01       2718.76  **
                     ** test_bbox.TB_008                 PASS          31.00           0.01       2247.78  **
                     ** test_bbox.TB_009                 PASS          31.00           0.01       2942.33  **
                     ** test_bbox.TB_010                 PASS          31.00           0.01       2850.98  **
                     ** test_bbox.TB_011                 PASS          31.00           0.01       3004.82  **
                     ** test_bbox.TB_012                 PASS          31.00           0.01       3475.37  **
                     ** test_bbox.TB_013                 PASS          31.00           0.01       3456.07  **
                     ** test_bbox.TB_014                 PASS          31.00           0.01       3373.66  **
                     ** test_bbox.TB_015                 PASS          31.00           0.01       3550.24  **
                     ** test_bbox.TB_016                 PASS          31.00           0.01       3376.73  **
                     ** test_bbox.TB_017                 PASS          31.00           0.01       2359.85  **
                     ** test_bbox.TB_018                 PASS          31.00           0.01       2238.54  **
                     ** test_bbox.TB_019                 PASS          31.00           0.01       2896.26  **
                     ** test_bbox.TB_020                 PASS          31.00           0.01       2264.03  **
                     ** test_bbox.TB_021                 PASS          31.00           0.01       2177.29  **
                     ** test_bbox.TB_022                 PASS          31.00           0.01       3010.87  **
                     ** test_bbox.TB_023                 PASS          31.00           0.01       2789.93  **
                     ** test_bbox.TB_024                 PASS          31.00           0.01       2877.54  **
                     ** test_bbox.TB_025                 PASS          31.00           0.01       3487.77  **
                     ** test_bbox.TB_026                 PASS          31.00           0.01       3371.30  **
                     ** test_bbox.TB_027                 PASS          31.00           0.01       3131.01  **
                     ** test_bbox.TB_028                 PASS          31.00           0.01       3502.80  **
                     ** test_bbox.TB_029                 PASS          31.00           0.01       3510.18  **
                     ** test_bbox.TB_030                 PASS          31.00           0.01       3517.49  **
                     ** test_bbox.TB_031                 PASS          31.00           0.01       3370.51  **
                     ** test_bbox.TB_032                 PASS          31.00           0.01       3024.04  **
                     ****************************************************************************************
                     ** TESTS=32 PASS=32 FAIL=0 SKIP=0                991.03           0.37       2687.17  **
                     ****************************************************************************************
```
- RV64
```bash
                     ****************************************************************************************
                     ** TEST                            STATUS  SIM TIME (ns)  REAL TIME (s)  RATIO (ns/s) **
                     ****************************************************************************************
                     ** test_bbox.TB_001                 PASS          30.00           0.01       2425.52  **
                     ** test_bbox.TB_002                 PASS          31.00           0.01       2737.02  **
                     ** test_bbox.TB_003                 PASS          31.00           0.01       3016.39  **
                     ** test_bbox.TB_004                 PASS          31.00           0.01       3179.16  **
                     ** test_bbox.TB_005                 PASS          31.00           0.01       3086.71  **
                     ** test_bbox.TB_006                 PASS          31.00           0.01       3058.90  **
                     ** test_bbox.TB_007                 PASS          31.00           0.01       3131.61  **
                     ** test_bbox.TB_008                 PASS          31.00           0.01       3101.58  **
                     ** test_bbox.TB_009                 PASS          31.00           0.01       3033.07  **
                     ** test_bbox.TB_010                 PASS          31.00           0.01       3169.01  **
                     ** test_bbox.TB_011                 PASS          31.00           0.01       2878.50  **
                     ** test_bbox.TB_012                 PASS          31.00           0.01       2980.85  **
                     ** test_bbox.TB_013                 PASS          31.00           0.01       2965.96  **
                     ** test_bbox.TB_014                 PASS          31.00           0.01       2995.27  **
                     ** test_bbox.TB_015                 PASS          31.00           0.01       3120.49  **
                     ** test_bbox.TB_016                 PASS          31.00           0.01       3207.39  **
                     ** test_bbox.TB_017                 PASS          31.00           0.01       3046.14  **
                     ** test_bbox.TB_018                 PASS          31.00           0.01       3074.52  **
                     ** test_bbox.TB_019                 PASS          31.00           0.01       2985.02  **
                     ** test_bbox.TB_020                 PASS          31.00           0.01       2917.64  **
                     ** test_bbox.TB_021                 PASS          31.00           0.01       3132.74  **
                     ** test_bbox.TB_022                 PASS          31.00           0.01       2895.74  **
                     ** test_bbox.TB_023                 PASS          31.00           0.01       2769.67  **
                     ** test_bbox.TB_024                 PASS          31.00           0.01       2648.17  **
                     ** test_bbox.TB_025                 PASS          31.00           0.01       2523.53  **
                     ** test_bbox.TB_026                 PASS          31.00           0.01       2703.73  **
                     ** test_bbox.TB_027                 PASS          31.00           0.01       3162.38  **
                     ** test_bbox.TB_028                 PASS          31.00           0.01       3128.22  **
                     ** test_bbox.TB_029                 PASS          31.00           0.01       2887.51  **
                     ** test_bbox.TB_030                 PASS          31.00           0.01       2861.59  **
                     ** test_bbox.TB_031                 PASS          31.00           0.01       2781.76  **
                     ** test_bbox.TB_032                 PASS          31.00           0.01       2968.87  **
                     ** test_bbox.TB_033                 PASS          31.00           0.01       2799.91  **
                     ** test_bbox.TB_034                 PASS          31.00           0.01       2783.84  **
                     ** test_bbox.TB_035                 PASS          31.00           0.01       2863.54  **
                     ** test_bbox.TB_036                 PASS          31.00           0.01       3016.46  **
                     ** test_bbox.TB_037                 PASS          31.00           0.01       3181.80  **
                     ** test_bbox.TB_038                 PASS          31.00           0.01       3072.12  **
                     ** test_bbox.TB_039                 PASS          31.00           0.01       3147.15  **
                     ** test_bbox.TB_040                 PASS          31.00           0.01       3083.71  **
                     ** test_bbox.TB_041                 PASS          31.00           0.01       3099.96  **
                     ** test_bbox.TB_042                 PASS          31.00           0.01       2816.52  **
                     ** test_bbox.TB_043                 PASS          31.00           0.01       2565.31  **
                     ****************************************************************************************
                     ** TESTS=43 PASS=43 FAIL=0 SKIP=0               1332.04           0.69       1941.14  **
                     ****************************************************************************************


```

#### Work Split up:
- Arun D A: instructions 1-15, 20-23
- AMS Arun Krishna: instructions 30-43
- Renish Israeli: instructions 16-29

### Test Results:
43 Out of 43 RV64 Instructions were implemented and executed correctly
![image-1.png](./image-1.png)

RV32 Instructions:
![image.png](./image.png)

