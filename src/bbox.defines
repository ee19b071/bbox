//See LICENSE.iitm for license details
/*

Author : Mouna Krishna
Email id : mounakrishna@mindgrovetech.in
Details: Has the macros used for instruction comparison

--------------------------------------------------------------------------------------------------
*/

// Add instruction macors here which can be used in the 
// case statement for understandability and easy modifications.

//Instruction - Bits: {[31:20],[14:12],[6:0]} are used to determine which operation to be performed


`ifdef RV64
    `define ADDUW     22'b0000100?????0000111011
    `define ANDN      22'b0100000?????1110110011
    `define BCLR      22'b0100100?????0010110011
    `define BCLRI     22'b010010??????0010010011
    `define BEXT      22'b0100100?????1010110011
    `define BEXTI     22'b010010??????1010010011
    `define BINV      22'b0110100?????0010110011
    `define BINVI     22'b011010??????0010010011
    `define BSET      22'b0010100?????0010110011
    `define BSETI     22'b001010??????0010010011
    `define CLMUL     22'b0000101?????0010110011
    `define CLMULH    22'b0000101?????0110110011
    `define CLMULR    22'b0000101?????0100110011
    `define CLZ       22'b0110000000000010010011
    `define CLZW      22'b0110000000000010011011
    `define CPOP      22'b0110000000100010010011
    `define CPOPW     22'b0110000000100010011011
    `define CTZ       22'b0110000000010010010011
    `define CTZW      22'b0110000000010010011011
    `define ORCB      22'b0010100001111010010011
    `define ORN       22'b0100000?????1100110011
    `define REV8      22'b0110101110001010010011
    `define ROL       22'b0110000?????0010110011
    `define ROLW      22'b0110000?????0010111011
    `define ROR       22'b0110000?????1010110011

    `define RORI      22'b011000??????1010010011
    `define RORIW     22'b0110000?????1010011011
    `define RORW      22'b0110000?????1010111011
    `define SEXTB     22'b0110000001000010010011
    `define SEXTH     22'b0110000001010010010011
    `define SH1ADD    22'b0010000?????0100110011
    `define SH1ADDUW  22'b0010000?????0100111011
    `define SH2ADD    22'b0010000?????1000110011
    `define SH2ADDUW  22'b0010000?????1000111011
    `define SH3ADD    22'b0010000?????1100110011
    `define SH3ADDUW  22'b0010000?????1100111011
    `define SLLIUW    22'b000010??????0010011011
    `define XNOR      22'b0100000?????1000110011
    `define ZEXTH     22'b0000100000001000111011

    `define MAX       22'b0000101?????1100110011
    `define MAXU      22'b0000101?????1110110011
    `define MIN       22'b0000101?????1000110011
    `define MINU      22'b0000101?????1010110011

`elsif RV32

    `define ANDN      22'b0100000?????1110110011
    `define BCLR      22'b0100100?????0010110011
    `define BCLRI     22'b0100100?????0010010011
    `define BEXT      22'b0100100?????1010110011
    `define BEXTI     22'b0100100?????1010010011
    `define BINV      22'b0110100?????0010110011
    `define BINVI     22'b0110100?????0010010011
    `define BSET      22'b0010100?????0010110011
    `define BSETI     22'b0010100?????0010010011
    `define CLMUL     22'b0000101?????0010110011
    `define CLMULH    22'b0000101?????0110110011
    `define CLMULR    22'b0000101?????0100110011
    `define CLZ       22'b0110000000000010010011
    `define CPOP      22'b0110000000100010010011
    `define CTZ       22'b0110000000010010010011
    `define ORCB      22'b0010100001111010010011
    `define ORN       22'b0100000?????1100110011
    `define REV8      22'b0110100110001010010011
    `define ROL       22'b0110000?????0010110011
    `define ROR       22'b0110000?????1010110011

    `define RORI      22'b0110000?????1010010011
    `define SEXTB     22'b0110000001000010010011
    `define SEXTH     22'b0110000001010010010011
    `define SH1ADD    22'b0010000?????0100110011
    `define SH2ADD    22'b0010000?????1000110011
    `define SH3ADD    22'b0010000?????1100110011
    `define XNOR      22'b0100000?????1000110011
    `define ZEXTH     22'b0000100000001000110011

    `define MAX       22'b0000101?????1100110011
    `define MAXU      22'b0000101?????1110110011
    `define MIN       22'b0000101?????1000110011
    `define MINU      22'b0000101?????1010110011

`else
`endif