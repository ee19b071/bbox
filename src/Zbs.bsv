// This instruction returns rs1 with a single bit cleared at the index specified in rs2. The index is read from the
// lower log2(XLEN) bits of rs2
function Bit#(XLEN) fn_bclr(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs1 & ~(1 << (rs2 & fromInteger(valueOf(XLEN)-1)));
endfunction

// This instruction returns a single bit extracted from rs1 at the index specified in rs2. The index is read from
// the lower log2(XLEN) bits of rs2.
function Bit#(XLEN) fn_bext(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return zeroExtend(rs1[rs2 & fromInteger(valueOf(XLEN)-1)]);
endfunction

// This instruction returns rs1 with a single bit inverted at the index specified in rs2. The index is read from the
// lower log2(XLEN) bits of rs2.
function Bit#(XLEN) fn_binv(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs1 ^ (1 << (rs2 & fromInteger(valueOf(XLEN)-1)));
endfunction

// This instruction returns rs1 with a single bit set at the index specified in rs2. The index is read from the
// lower log2(XLEN) bits of rs2.
function Bit#(XLEN) fn_bset(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs1 | (1 << (rs2 & fromInteger(valueOf(XLEN)-1)));
endfunction


