//This instruction performs an XLEN-wide addition between rs2 and the zero-extended least-significant word of rs1.
function Bit#(XLEN) fn_adduw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  rs2 = zeroExtend(rs2[31:0]);
  return rs1 + rs2;
endfunction

//This instruction shifts rs1 to the left by 1 bit and adds it to rs2.
function Bit#(XLEN) fn_sh1add(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs2 + (rs1 << 1);
endfunction

// This instruction performs an XLEN-wide addition of two addends. The first addend is rs2. The second
// addend is the unsigned value formed by extracting the least-significant word of rs1 and shifting it left by 1
// place.
function Bit#(XLEN) fn_sh1adduw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(32) temp = truncate(rs1);
  Bit#(XLEN) index = zeroExtend(temp);
  return rs2 + (index << 1);
endfunction

// This instruction shifts rs1 to the left by 2 places and adds it to rs2
function Bit#(XLEN) fn_sh2add(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs2 + (rs1 << 2);
endfunction

// This instruction performs an XLEN-wide addition of two addends. The first addend is rs2. The second
// addend is the unsigned value formed by extracting the least-significant word of rs1 and shifting it left by 2
// places.
function Bit#(XLEN) fn_sh2adduw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(32) temp = truncate(rs1);
  Bit#(XLEN) index = zeroExtend(temp);
  return rs2 + (index << 2);
endfunction

// This instruction shifts rs1 to the left by 3 places and adds it to rs2
function Bit#(XLEN) fn_sh3add(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs2 + (rs1 << 3);
endfunction

// This instruction performs an XLEN-wide addition of two addends. The first addend is rs2. The second
// addend is the unsigned value formed by extracting the least-significant word of rs1 and shifting it left by 3
// places.
function Bit#(XLEN) fn_sh3adduw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(32) temp = truncate(rs1);
  Bit#(XLEN) index = zeroExtend(temp);
  return rs2 + (index << 3);
endfunction

// This instruction takes the least-significant word of rs1, zero-extends it, and shifts it left by the immediate.
function Bit#(XLEN) fn_slliuw(Bit#(XLEN) rs1, Bit#(6) shamt_temp);
  Bit#(32) temp = truncate(rs1);
  Bit#(XLEN) index = zeroExtend(temp);
  Bit#(XLEN) shamt = zeroExtend(shamt_temp);
  return index << shamt;
endfunction
