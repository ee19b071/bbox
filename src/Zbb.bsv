// This instruction performs the bitwise logical AND operation between rs1 and the bitwise inversion of rs2.
function Bit#(XLEN) fn_andn(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return rs1 & ~rs2;
endfunction

// This instruction counts the number of 0’s before the first 1, starting at the most-significant bit (i.e., XLEN-1)
// and progressing to bit 0. Accordingly, if the input is 0, the output is XLEN, and if the most-significant bit of
// the input is a 1, the output is 0.
function Bit#(XLEN) fn_clz(Bit#(XLEN) rs1);
  return zeroExtend(pack(countZerosMSB(rs1)));
endfunction

// This instruction counts the number of 0’s before the first 1 starting at bit 31 and progressing to bit 0.
// Accordingly, if the least-significant word is 0, the output is 32, and if the most-significant bit of the word
// (i.e., bit 31) is a 1, the output is 0.
function Bit#(XLEN) fn_clzw(Bit#(XLEN) rs1);
  return zeroExtend(pack(countZerosMSB(rs1[31:0])));
endfunction

// The function fn_cpop finds the total number of set bits in the register file location pointed by rs1
function Bit#(XLEN) fn_cpop(Bit#(XLEN) rs1);
  return zeroExtend(pack(countOnes(rs1)));
endfunction

// The function fn_cpopw finds the total number of set bits in the lower word (32-bit) of the register file location pointed by rs1
function Bit#(XLEN) fn_cpopw(Bit#(XLEN) rs1);
  return zeroExtend(pack(countOnes(rs1[31:0])));
endfunction

// The function fn_ctz finds the total number of trailing zeros in the register file location pointed by rs1
function Bit#(XLEN) fn_ctz(Bit#(XLEN) rs1);
  return zeroExtend(pack(countZerosLSB(rs1)));
endfunction

// The function fn_cpopw finds the total number of trailing zeros in the lower word (32-bit) of the register file location pointed by rs1
function Bit#(XLEN) fn_ctzw(Bit#(XLEN) rs1);
  return zeroExtend(pack(countZerosLSB(rs1[31:0])));
endfunction

// Combines the bits within each byte using bitwise logical OR. This sets the bits of each byte in the result rd
// to all zeros if no bit within the respective byte of rs is set, or to all ones if any bit within the respective byte
// of rs is set.
function Bit#(XLEN) fn_orcb(Bit#(XLEN) rs1);
  Integer k=8;
  Bit#(XLEN) result=0;
  Bit#(8) temp;
  for(Integer j=valueOf(XLEN)/8; j>0 ; j=j-1)
  begin
    temp = rs1[k-1:k-8];
    if(temp == 8'b00000000)
    begin
      result[k-1:k-8] = 8'b00000000;
    end
    
    else
    begin
      result[k-1:k-8] = 8'b11111111;
    end
    
    k=k+8;
  end
  
  return result;
endfunction

// This instruction performs the bitwise logical AND operation between rs1 and the bitwise inversion of rs2.
function Bit#(XLEN) fn_orn(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(XLEN) result = (rs1 | (~rs2));
  return result;
endfunction

// This instruction reverses the order of the bytes in rs
function Bit#(XLEN) fn_rev8(Bit#(XLEN) rs1);
  Bit#(XLEN) inp = rs1;
  Bit#(XLEN) result = 0;
  Integer j = valueOf(XLEN)-1;
  Bit#(8) temp;
  for(Integer i = 0; i <= (valueOf(XLEN)-8); i=i+8)
  begin
    temp = inp[j:j-7];
    result[i+7:i] = temp;
    j = j-8;
  end
  return result;
endfunction

// This instruction performs a rotate left of rs1 by the amount in least-significant log2(XLEN) bits of rs2.
function Bit#(XLEN) fn_rol(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  `ifdef RV32  Bit#(XLEN) shamt=zeroExtend(rs2[4:0]);
  `elsif RV64  Bit#(XLEN) shamt=zeroExtend(rs2[5:0]);
  `endif
  return (rs1 << shamt) | (rs1 >> (fromInteger(valueOf(XLEN))-shamt));
endfunction

// This instruction performs a rotate left on the least-significant word of rs1 by the amount in least-significant 5
// bits of rs2. The resulting word value is sign-extended by copying bit 31 to all of the more-significant bits.
function Bit#(XLEN) fn_rolw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(32) temp=rs1[31:0];
  Bit#(32) shamt=zeroExtend(rs2[4:0]);
  Bit#(32) result_temp = (temp << shamt) |  (temp >> 32-shamt);
  return signExtend(result_temp);
endfunction

// This instruction performs a rotate right of rs1 by the amount in least-significant log2(XLEN) bits of rs2
function Bit#(XLEN) fn_ror(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  `ifdef RV32  Bit#(XLEN) shamt=zeroExtend(rs2[4:0]);
  `elsif RV64  Bit#(XLEN) shamt=zeroExtend(rs2[5:0]);
  `endif
  return (rs1 >> shamt) | (rs1 << (fromInteger(valueOf(XLEN))-shamt));
endfunction

//  This instruction performs a rotate right of rs1 by the amount in the least-significant log2(XLEN) bits of
// shamt. For RV32, the encodings corresponding to shamt[5]=1 are reserved.
function Bit#(XLEN) fn_rori(Bit#(XLEN) rs1, Bit#(XLEN) shamt);
  return zeroExtend(rs1 >> shamt) | zeroExtend(rs1 << (fromInteger(valueOf(XLEN)) - shamt));
endfunction

// This instruction performs a rotate right on the least-significant word of rs1 by the amount in the leastsignificant log2(XLEN) bits of shamt. The resulting word value is sign-extended by copying bit 31 to all of
// the more-significant bits.
function Bit#(XLEN) fn_roriw(Bit#(XLEN) rs1, Bit#(5) shamt_temp);
  Bit#(32) rs1_data = truncate(rs1);
  Bit#(32) shamt = zeroExtend(shamt_temp);
  Bit#(32) result = truncate((rs1_data >> shamt) | (rs1_data << (32 - shamt)));
  return signExtend(result);
endfunction

// This instruction performs a rotate right on the least-significant word of rs1 by the amount in least-significant
// 5 bits of rs2. The resultant word is sign-extended by copying bit 31 to all of the more-significant bits.
function Bit#(XLEN) fn_rorw(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(32) rs1_data = truncate(rs1);
  Bit#(5)  shamt_temp = truncate(rs2);
  Bit#(32) shamt = zeroExtend(shamt_temp);
  Bit#(32) result = truncate((rs1_data >> shamt) | (rs1_data << (32 - shamt)));
  return signExtend(result);
endfunction

// This instruction sign-extends the least-significant byte in the source to XLEN by copying the most-significant
// bit in the byte (i.e., bit 7) to all of the more-significant bits
function Bit#(XLEN) fn_sextb(Bit#(XLEN) rs1);
  Bit#(8) temp = truncate(rs1);
  return signExtend(temp);
endfunction

// This instruction sign-extends the least-significant halfword in rs to XLEN by copying the most-significant bit
// in the halfword (i.e., bit 15) to all of the more-significant bits.
function Bit#(XLEN) fn_sexth(Bit#(XLEN) rs1);
  Bit#(16) temp = truncate(rs1);
  return signExtend(temp);
endfunction

// This instruction performs the bit-wise exclusive-NOR operation on rs1 and rs2
function Bit#(XLEN) fn_xnor(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  return ~(rs1 ^ rs2);
endfunction

// This instruction zero-extends the least-significant halfword of the source to XLEN by inserting 0’s into all of
// the bits more significant than 15.
function Bit#(XLEN) fn_zexth(Bit#(XLEN) rs1);
  Bit#(16) temp = truncate(rs1);
  return zeroExtend(temp);
endfunction

function Bit#(XLEN) fn_max(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Int#(XLEN) r1 = unpack(rs1);
  Int#(XLEN) r2 = unpack(rs2);
  if(r1 < r2)
    return rs2;
  else
    return rs1;
endfunction

function Bit#(XLEN) fn_maxu(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  UInt#(XLEN) r1 = unpack(rs1);
  UInt#(XLEN) r2 = unpack(rs2);
  return (rs1 < rs2)?rs2:rs1;
endfunction

function Bit#(XLEN) fn_min(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Int#(XLEN) r1 = unpack(rs1);
  Int#(XLEN) r2 = unpack(rs2);
  if(r1 < r2)
    return rs1;
  else
    return rs2;
endfunction

function Bit#(XLEN) fn_minu(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  UInt#(XLEN) r1 = unpack(rs1);
  UInt#(XLEN) r2 = unpack(rs2);
  if(rs1 < rs2)
    return rs1;
  else
    return rs2;
endfunction
