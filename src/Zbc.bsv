// clmul produces the lower half of the 2·XLEN carry-less product
 function Bit#(XLEN) fn_clmul(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(XLEN) result = 0;
  for(Integer i = 0; i < valueOf(XLEN); i=i+1)
  begin
    if(((rs2>>i) & 1) == 1)
    begin
      result = result ^ (rs1<<i);
    end
  end
  return signExtend(result);
endfunction

// clmulh produces the upper half of the 2·XLEN carry-less product
function Bit#(XLEN) fn_clmulh(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(XLEN) result = 0;
  for(Integer i = 1; i < valueOf(XLEN); i=i+1)
  begin
    if(((rs2>>i) & 1) == 1)
    begin
      result = result ^ (rs1>>(valueOf(XLEN)-i));
    end
  end
  return signExtend(result);
endfunction

// clmulr produces bits 2·XLEN−2:XLEN-1 of the 2·XLEN carry-less product
function Bit#(XLEN) fn_clmulr(Bit#(XLEN) rs1, Bit#(XLEN) rs2);
  Bit#(XLEN) result = 0;
  for(Integer i = 0; i < valueOf(XLEN)-1; i=i+1)
  begin
    if(((rs2>>i) & 1) == 1)
    begin
      result = result ^ (rs1>>(valueOf(XLEN)-i-1));
    end
  end
  return signExtend(result);
endfunction

